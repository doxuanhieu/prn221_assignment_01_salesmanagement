﻿using BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ConsoleApp1
{
    public class Program
    {
        static void findAllProduct()
        {
            IProductRepository repository = new ProductRepository();
            List<Product> list = repository.Search();

            foreach (Product product in list)
            {
                Debug.WriteLine(product.ToString());
            }
        }

        static void findAllMember()
        {
            IMemberRepository repository = new MemberRepository();
            List<Member> list = repository.Search();

            foreach (Member member in list)
            {
                Debug.WriteLine(member.ToString());
            }
        }

        static void findAllOrder()
        {
            IOrderRepository repository = new OrderRepository();
            List<Order> list = repository.Search();

            foreach (Order order in list)
            {
                Debug.WriteLine(order.ToString());
            }
        }

        static void findOneOrderDetailByOrderId(int orderId)
        {
            IOrderDetailRepository repository = new OrderDetailRepository();
            List<OrderDetail> orderDetails = repository.Search(orderId);

            decimal total = 0;

            foreach (OrderDetail orderDetail in orderDetails)
            {
                Debug.WriteLine(orderDetail.ToString());
                total += ((orderDetail.Quantity * orderDetail.UnitPrice) - (decimal)(orderDetail.Discount));
            }
            Debug.WriteLine("Total Amount: " + total + "$");
        }

        static int addMember(Member member)
        {
            IMemberRepository repository = new MemberRepository();
            return repository.Create(member);
        }

        static void Main(string[] args)
        {
            Member member= new Member();

            member.Email = "123@gmail.com";
            member.CompanyName = "FPTU";
            member.City = "FPTU";
            member.Country = "FPTU";
            member.Password = "FPTU";

            Console.WriteLine(addMember(member));
        }
    }
}

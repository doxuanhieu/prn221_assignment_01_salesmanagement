﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IMemberRepository
    {
        int login(string username, string password);

        int Create(Member member);

        int Update(Member member);

        int Delete(int memberId);

        List<Member> Search();

        Member findById(int memberId);
    }
}

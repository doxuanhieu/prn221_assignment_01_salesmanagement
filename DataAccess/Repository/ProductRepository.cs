﻿using BusinessObject;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.CompilerServices.RuntimeHelpers;
using System.Xml.Linq;
using System.Diagnostics;

namespace DataAccess.Repository
{
    public class ProductRepository : IProductRepository
    {
        public int Create(Product product)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {

                    Product checkProductName = context.Products.FirstOrDefault(x => x.ProductName.Equals(product.ProductName));

                    if (checkProductName == null)
                    {
                        context.Products.Add(product);
                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }

        public int Delete(int productId)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Product checkExist = context.Products.Find(productId);

                    if (checkExist != null)
                    {
                        context.Products.Remove(checkExist);
                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }

        public Product findById(int productId)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Product product = context.Products.Find(productId);

                    if (product != null)
                    {
                        return product;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public List<Product> Search()
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    List<Product> list = context.Products.ToList();
                    return list;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public int Update(Product updateProduct)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Product checkExist = context.Products.Find(updateProduct.ProductId);

                    if (checkExist != null)
                    {
                        checkExist.CategoryId = updateProduct.CategoryId;
                        checkExist.ProductName = updateProduct.ProductName;
                        checkExist.Weight = updateProduct.Weight;
                        checkExist.UnitPrice = updateProduct.UnitPrice;
                        checkExist.UnitsInStock = updateProduct.UnitsInStock;

                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }catch (Exception ex) { 
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }
    }
}

﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DataAccess.Repository
{
    public class OrderDetailRepository : IOrderDetailRepository
    {
        public int Create(OrderDetail orderDetail)
        {
            throw new NotImplementedException();
        }

        public int Delete(int orderDetailId)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    OrderDetail checkExist = context.OrderDetails.Find(orderDetailId);

                    if (checkExist != null)
                    {
                        context.OrderDetails.Remove(checkExist);
                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }


        public List<OrderDetail> Search(int orderId)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    List<OrderDetail> list  = context.OrderDetails.Where(x => x.OrderId== orderId).ToList();

                    if(list != null)
                    {
                        return list;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public int Update(OrderDetail updateOrderDetail)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    OrderDetail checkExist = context.OrderDetails.Where(e => e.OrderId.Equals(updateOrderDetail.OrderId)).FirstOrDefault();

                    if (checkExist != null)
                    {
                        checkExist.Quantity = updateOrderDetail.Quantity;
                        checkExist.UnitPrice = updateOrderDetail.UnitPrice;
                        checkExist.Discount = updateOrderDetail.Discount;

                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }
    }
}

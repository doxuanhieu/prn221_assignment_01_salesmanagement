﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace DataAccess.Repository
{
    public class OrderRepository : IOrderRepository
    {
        public int Create(Order order)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    //Order checkExist = context.Orders.FirstOrDefault(x => x.Email.Equals(member.Email));
                    Order checkExist = null;

                    if (checkExist == null)
                    {
                        context.Orders.Add(order);
                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }

        public int Delete(int orderId)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Order checkExist = context.Orders.Find(orderId);

                    if (checkExist != null)
                    {
                        context.Orders.Remove(checkExist);
                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }

        public List<Order> Search()
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    List<Order> list = context.Orders.ToList();
                    return list;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public int Update(Order updateOrder)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Order checkExist = context.Orders.Find(updateOrder.OrderId);

                    if (checkExist != null)
                    {
                        checkExist.OrderDate = updateOrder.OrderDate;
                        checkExist.RequiredDate = updateOrder.RequiredDate;
                        checkExist.ShippedDate = updateOrder.ShippedDate;
                        checkExist.Freight = updateOrder.Freight;

                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }
    }
}

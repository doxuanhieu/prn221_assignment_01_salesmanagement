﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IOrderRepository
    {
        int Create(Order order);

        int Update(Order order);

        int Delete(int orderId);

        List<Order> Search();
    }
}

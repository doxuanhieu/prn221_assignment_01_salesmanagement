﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.CompilerServices.RuntimeHelpers;
using System.Xml.Linq;
using System.Diagnostics;

namespace DataAccess.Repository
{
    public class MemberRepository : IMemberRepository
    {
        public int Create(Member member)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Member checkEmail = context.Members.FirstOrDefault(x => x.Email.Equals(member.Email));

                    if (checkEmail == null)
                    {
                        context.Members.Add(member);
                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }

        public int Delete(int memberId)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Member member = context.Members.Find(memberId);

                    if (member != null)
                    {
                        context.Members.Remove(member);
                        context.SaveChanges();
                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }

        }

        public Member findById(int memberId)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Member member = context.Members.Find(memberId);

                    if (member != null)
                    {
                        return member;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public int login(string username, string password)
        {
            try
            {
                if (username.Trim().Length == 0 || password.Trim().Length == 0)
                {
                    return Const.FAILED;
                }

                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Member employee = context.Members.Where(e => e.Email.Equals(username)).FirstOrDefault();

                    if (employee != null && employee.Password.Equals(password))
                    {

                        return Const.SUCCESS;
                    }
                    else
                    {
                        return Const.FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }

        public List<Member> Search()
        {
            try {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    //dgvSinhVien.DataSource = context.Students.ToList();
                    List<Member> list = context.Members.ToList();
                    return list;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public int Update(Member updateMember)
        {
            try
            {
                using (var context = new PRN221_Assignment_01_SalesManagementContext())
                {
                    Member member = context.Members.Find(updateMember.MemberId);

                    member.CompanyName = updateMember.CompanyName;
                    member.City = updateMember.City;
                    member.Country = updateMember.Country;

                    context.SaveChanges();

                    return Const.SUCCESS;
                }
            }
            catch (Exception ex) { 
                Debug.WriteLine(ex.Message);
                return Const.FAILED;
            }
        }
    }
}

﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IOrderDetailRepository
    {
        int Create(OrderDetail orderDetail);

        int Update(OrderDetail orderDetail);

        int Delete(int orderDetailId);

        List<OrderDetail> Search(int orderId);
    }
}

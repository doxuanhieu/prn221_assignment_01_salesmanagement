﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IProductRepository
    {
        int Create(Product product);

        int Update(Product product);

        int Delete(int productId);

        List<Product> Search();

        Product findById(int productId);
    }
}

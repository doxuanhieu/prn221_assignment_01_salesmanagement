﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class Const
    {
        public const string NOT_AUTHORIZED = "Not authorized.";
        public const string INVALID_DATA  = "INVALID DATA.";

        public const int SUCCESS = 1;
        public const int FAILED = 0;

        public const string SYSTEM_ERROR = "System hava error, please try again.";

        public const string UPDATE_SUCCESSFULLY = "Update successfully.";
        public const string UPDATE_FAILED = "Update hava error, please try again.";

        public const string DELETE_SUCCESSFULLY = "Delete successfully.";
        public const string DELETE_FAILED = "Delete hava error, please try again.";

        public const string CREATE_SUCCESSFULLY = "Create successfully.";
        public const string CREATE_FAILED = "Create hava error, please try again.";

        public const string CONFIRM_DELETE = "Are you sure you want to delete this item?";

        public const int CREATE_MODE = -47853;
    }
}

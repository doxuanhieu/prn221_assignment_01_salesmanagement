﻿using DataAccess;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Windows.Shapes;

namespace SalesWPFApp
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        IMemberRepository repository = new MemberRepository();
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string username = txtEmail.Text;
                string password = txtPassword.Password.ToString();

                if (username != null && password != null)
                {
                    int result = repository.login(username, password);

                    if (result == Const.SUCCESS)
                    {

                        //MessageBox.Show("Login successfully.", "Success");

                        ProductWindow productWindow = new ProductWindow();
                        this.Close();
                        productWindow.Show();
                    }
                    else
                    {
                        MessageBox.Show("Login failed.", "Failed");
                    }
                }
                else
                {
                    MessageBox.Show("Login failed.", "Failed");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                MessageBox.Show("Login failed.", "Failed");
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //protected override void OnClosing(CancelEventArgs e)
        //{
        //    if (MessageBox.Show(this, "Do you want exit?", "Confirm", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
        //    {
        //        e.Cancel = true;
        //    }
        //}

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
    }
}

﻿using BusinessObject;
using DataAccess;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalesWPFApp
{
    /// <summary>
    /// Interaction logic for EditProductWindow.xaml
    /// </summary>
    public partial class EditProductWindow : Window
    {
        IProductRepository repository = new ProductRepository();

        public EditProductWindow(int productId)
        {
            InitializeComponent();
            LoadData(productId);
        }

        void LoadData(int productId)
        {
            try
            {
                if (productId == Const.CREATE_MODE)
                {
                    //Create Product
                    btnUpdate.Visibility = Visibility.Collapsed;
                    labelProductId.Visibility = Visibility.Collapsed;
                    txtProductId.Visibility = Visibility.Collapsed;

                }
                else
                {
                    //Update Product
                    btnInsert.Visibility = Visibility.Collapsed;

                    Product product = repository.findById(productId);

                    txtProductId.Text = product.ProductId.ToString();
                    txtCategoryId.Text = product.CategoryId.ToString();
                    txtProductName.Text = product.ProductName.ToString();
                    txtWeight.Text = product.Weight.ToString();
                    txtUnitPrice.Text = product.UnitPrice.ToString();
                    txtUnitsInStock.Text = product.UnitsInStock.ToString();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                MessageBox.Show("Load data hava error, please try again.", "Error");
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            ProductWindow frm = new ProductWindow();
            this.Close();
            frm.Show();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Product updateProduct = new Product();

                updateProduct.ProductId = Convert.ToInt32(txtProductId.Text);
                updateProduct.CategoryId = Convert.ToInt32(txtCategoryId.Text);
                updateProduct.ProductName = txtProductName.Text;
                updateProduct.Weight = txtWeight.Text;
                updateProduct.UnitPrice = Convert.ToDecimal(txtUnitPrice.Text);
                updateProduct.UnitsInStock = Convert.ToInt32(txtUnitsInStock.Text);

                int result = repository.Update(updateProduct);

                if (result == Const.SUCCESS)
                {
                    btnCancel_Click(sender, e);
                    MessageBox.Show(Const.UPDATE_SUCCESSFULLY, "Success");
                }
                else
                {
                    btnCancel_Click(sender, e);
                    MessageBox.Show(Const.UPDATE_FAILED, "Error");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                btnCancel_Click(sender, e);
                MessageBox.Show(Const.UPDATE_FAILED, "Error");
            }
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("EditProductWindow.btnInsert_Click");
            try
            {
                Product newProduct = new Product();

                newProduct.CategoryId = Convert.ToInt32(txtCategoryId.Text.Trim('.'));
                newProduct.ProductName = txtProductName.Text;
                newProduct.Weight = txtWeight.Text;
                newProduct.UnitPrice = Convert.ToDecimal(txtUnitPrice.Text.Trim('.'));
                newProduct.UnitsInStock = Convert.ToInt32(txtUnitsInStock.Text.Trim('.'));

                int result = repository.Create(newProduct);

                if (result == Const.SUCCESS)
                {
                    btnCancel_Click(sender, e);
                    MessageBox.Show(Const.CREATE_SUCCESSFULLY, "Success");
                }
                else
                {
                    MessageBox.Show(Const.CREATE_FAILED, "Error");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                MessageBox.Show(Const.CREATE_FAILED, "Error");
            }
        }
    }
}

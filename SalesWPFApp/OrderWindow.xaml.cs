﻿using BusinessObject;
using DataAccess;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalesWPFApp
{
    /// <summary>
    /// Interaction logic for OrderWindow.xaml
    /// </summary>
    public partial class OrderWindow : Window
    {
        IOrderRepository repository = new OrderRepository();

        public OrderWindow()
        {
            InitializeComponent();
            loadData();
        }

        void loadData()
        {
            dataGrid.ItemsSource = repository.Search();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var messageBoxResult = MessageBox.Show(Const.CONFIRM_DELETE, "Confirmation", MessageBoxButton.YesNo);

                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    Member member = (Member)((Button)e.Source).DataContext;

                    int result = repository.Delete(member.MemberId);

                    if (result == Const.SUCCESS)
                    {
                        loadData();
                        MessageBox.Show(Const.DELETE_SUCCESSFULLY, "Sucess");
                    }
                    else
                    {
                        MessageBox.Show(Const.DELETE_FAILED, "Error");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                MessageBox.Show(Const.SYSTEM_ERROR, "Error");
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Member member = (Member)((Button)e.Source).DataContext;

                DetailMemberWindow frm = new DetailMemberWindow(member.MemberId);
                this.Hide();
                frm.Show();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                MessageBox.Show(Const.SYSTEM_ERROR, "Error");
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            DetailMemberWindow frm = new DetailMemberWindow(Const.CREATE_MODE);
            this.Hide();
            frm.Show();
        }

        private void btnMember_Click(object sender, RoutedEventArgs e)
        {
            //No work
        }

        private void btnProduct_Click(object sender, RoutedEventArgs e)
        {
            ProductWindow productWindow = new ProductWindow();
            this.Close();
            productWindow.Show();
        }

        private void btnOrder_Click(object sender, RoutedEventArgs e)
        {
            ProductWindow productWindow = new ProductWindow();
            this.Close();
            productWindow.Show();
        }
    }
}

﻿using BusinessObject;
using DataAccess.Repository;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalesWPFApp
{
    /// <summary>
    /// Interaction logic for DetailMemberWindow.xaml
    /// </summary>
    public partial class DetailMemberWindow : Window
    {
        IMemberRepository repository = new MemberRepository();

        public DetailMemberWindow(int memberId)
        {
            InitializeComponent();
            LoadData(memberId);
        }

        void LoadData(int memberId)
        {
            try
            {
                if (memberId == Const.CREATE_MODE)
                {
                    //Create Product
                    btnUpdate.Visibility = Visibility.Collapsed;
                    labelMemberId.Visibility = Visibility.Collapsed;
                    txtMemberId.Visibility = Visibility.Collapsed;

                }
                else
                {
                    //Update Product
                    btnInsert.Visibility = Visibility.Collapsed;
                    txtEmail.IsReadOnly = true;

                    Member member = repository.findById(memberId);

                    txtMemberId.Text = member.MemberId.ToString();
                    txtEmail.Text = member.Email;
                    txtCompanyName.Text = member.CompanyName;
                    txtCity.Text = member.City;
                    txtCountry.Text = member.Country;
                    txtPassword.Text = member.Password;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                MessageBox.Show("Load data hava error, please try again.", "Error");
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            MemberWindow frm = new MemberWindow();
            this.Close();
            frm.Show();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Member updateMember = new Member();

                updateMember.MemberId = Convert.ToInt32(txtMemberId.Text);
                updateMember.Email = txtEmail.Text;
                updateMember.CompanyName = txtCompanyName.Text;
                updateMember.City = txtCity.Text;
                updateMember.Country = txtCountry.Text;
                updateMember.Password = txtPassword.Text;

                int result = repository.Update(updateMember);

                if (result == Const.SUCCESS)
                {
                    btnCancel_Click(sender, e);
                    MessageBox.Show(Const.UPDATE_SUCCESSFULLY, "Success");
                }
                else
                {
                    btnCancel_Click(sender, e);
                    MessageBox.Show(Const.UPDATE_FAILED, "Error");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                btnCancel_Click(sender, e);
                MessageBox.Show(Const.UPDATE_FAILED, "Error");
            }
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("EditProductWindow.DetailMemberWindow");
            try
            {
                Member newMember = new Member();

                newMember.Email = txtEmail.Text;
                newMember.CompanyName = txtCompanyName.Text;
                newMember.City = txtCity.Text;
                newMember.Country = txtCountry.Text;
                newMember.Password = txtPassword.Text;

                int result = repository.Create(newMember);

                if (result == Const.SUCCESS)
                {
                    btnCancel_Click(sender, e);
                    MessageBox.Show(Const.CREATE_SUCCESSFULLY, "Success");
                }
                else
                {
                    MessageBox.Show(Const.CREATE_FAILED, "Error");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
                MessageBox.Show(Const.CREATE_FAILED, "Error");
            }
        }
    }
}
